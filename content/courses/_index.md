---
title: Courses
layout: docs  # Do not modify.

# Optional header image (relative to `static/img/` folder).
header:
  caption: ""
  image: ""
---

I have long been frustrated with the unwilling-ness of several educators to update their practices to using internet-based tools.
While I am not a professional educator at all, I do hope that my expertise in the following material can give you (the reader) some help in learning it.

> Don't expect to learn everything from this site. You will need to be proactive by looking through the resources I have included or even searching for your own.
