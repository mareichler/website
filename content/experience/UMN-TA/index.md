---
title: Physics Teaching Assistant  
subtitle: University of Minnesota (Sep 2018 - Present)
summary: Sep 2018 - Present
tags:
- teaching 
#set date, this will affect ordering 
date:    "2018-09-01T00:00:00Z"
#update with last modify (based on commits to github page for this file)
lastmod: "2019-04-17T00:00:00Z"


# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  placement: 2
  caption: ''
  focal_point: ""
  preview_only: true

links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: 
---

The University of Minnesota's Physic Education research group has developed a method of physics instruction called the [Minnesota Model](https://groups.physics.umn.edu/physed/Research/MNModel/MMt.html).
It offers a multi-faceted approached based off of peer-reviewed research that I have found to greatly benefit the students.

My part in the Minnesota Model as a Teaching Assitant is to be a "coach": someone guiding the students through difficult problems without just giving them the answer.
I have found this task to be very complicated because each student requires a different level of coaching in order to be successful;
nevertheless, I have found great joy helping my students learn and learning from them.
This position gave me an opportunity to interface with students from diverse backgrounds all while we focused on the common goal of learning physics.

More specifically, my role included testing, organizing, and facilitating group problem-solving and experimental practic sessions; both focusing on encouraging the students to struggle with the material in order to gain a deeper understanding of it.
As a part of the experimental sessions, I assigned reports every two to three weeks that I could use to evaluate how the students' were progressing and how deep their understanding of a specific concept was.

On this site, I have a <a href="/mywork/#teaching" > list of TA courses and reviews </a> from my experience at University of Minnesota.
