---
title: Passive Piezoelectric Energy Generation
subtitle: Hamline University (Summer 2015)
summary: Summer 2015
tags:
- research
#set date 
date:    "2015-08-01T00:00:00Z"
#update with last modify (based on commits to github page for this file)
lastmod: "2019-04-17T00:00:00Z"


# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  placement: 2
  caption: ''
  focal_point: ""
  preview_only: true

links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: example
---

This was my first summer at Hamline University, and my first full fledged research project.

I worked on developing prototype renewable energy source, specifically focusing on using piezoelectrics to harvest energy from foot steps.
This project was very fun and only made me excited to research more in the future.

My advisor for this project was [Prof. Bruce Bolon](https://www.hamline.edu/faculty-staff/bruce-bolon/) from Hamline University.


