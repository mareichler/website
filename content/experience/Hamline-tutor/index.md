---
title: Academic Support Tutor  
subtitle: Hamline University (Sep 2015 - May 2018)
summary: Sep 2015 - May 2018
tags:
- teaching 
#set date, this will affect ordering 
date:    "2018-05-01T00:00:00Z"
#update with last modify (based on commits to github page for this file)
lastmod: "2019-04-17T00:00:00Z"


# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  placement: 2
  caption: ''
  focal_point: ""
  preview_only: true

links:
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: 
---

I began my work as a tutor with excitement.
I had noticed early on in my academic career that I enjoyed educating my peers, so having this opportunity was a delightful learning experience.

While tutoring, I not only instructed my fellow students in complicated concepts from the Physics, Mathematics, and Computer Science curricula;
I also was able to make positive long-lasting connections with them.
Moreover, this experience really showed me how to work through complicated problems myself by forcing me to break down my own problem-solving strategy in a way that is explainable to a confused student.

I have brought this experience with me throughout my career until now.
I pride myself on breaking down complicated concepts in ways that are easier to understand whether I am actually teaching physics or documenting a complex piece of software.
