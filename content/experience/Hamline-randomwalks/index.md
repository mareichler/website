---
title: Random Walks Investigation 
subtitle: Hamline University Honors Thesis (Sep 2017 - May 2018)
summary: Sep 2017 - May 2018
tags:
- research
#set date 
date:    "2018-05-01T00:00:00Z"
#update with last modify (based on commits to github page for this file)
lastmod: "2019-04-17T00:00:00Z"


# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  placement: 2
  caption: ''
  focal_point: ""
  preview_only: true 

links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
url_code: "https://gitlab.com/tbeichlersmith/RandomManifoldWalks"
url_pdf: "https://digitalcommons.hamline.edu/dhp/70/"
url_slides: "https://gitlab.com/tbeichlersmith/RandomManifoldWalks/-/blob/master/Presentation/slides.pdf?raw=true"
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: 
---

This project was the culmination of my undergraduate career at Hamline University.
I had just returned from my REU at CERN, and I was excited to continue expanding my knowledge of computational mathematics.

In this project, I developed a C++ library and several executables that efficiently perform random walks on several types of two-dimensional manifolds. The project defines the manifolds by their [Christoffel Symbols](https://en.wikipedia.org/wiki/Christoffel_symbols) and defines an "escape condition" to allow for some characterization of the length of the walks. This problem on how long random walks take to get to an "escape region" can be recharcterized as a numerical solution to the differential equation $\Delta u + 1 = 0$ on the chosen manifold with Dirichlet boundary conditions at the boundary of the escape region.

[Mathematica](https://www.wolfram.com/mathematica/) was used to validate the behavior of the C++ library on the different manifolds, but then C++ was used to do the majority of data-generation. The [R](https://www.r-project.org/) programming language along with the [ggplot2](https://ggplot2.tidyverse.org/) package was used to construct the plots.

I enjoyed this project and I still think about it to this day. It left a solid mark on me about the powers of how much I can learn by doing simulated experiments. Below is a selection of plots that I made with this project to inspect the preliminary results.

 {{< gallery album="gallery" >}}
 
 My advisor for this project was [Prof. Arthur Guetter](https://www.hamline.edu/faculty-staff/arthur-guetter/) from Hamline University.
