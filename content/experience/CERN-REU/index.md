---
title: ATLAS Forward Proton Project 
subtitle: CERN REU (Summer 2017)
summary: Summer 2017
tags:
- research
- high energy physics
- REU
#set date 
date:    "2017-08-01T00:00:00Z"
#update with last modify (based on commits to github page for this file)
lastmod: "2019-04-17T00:00:00Z"


# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 2
  caption: 'Image source: [**Cureus**](https://cureusresearch.files.wordpress.com/2011/02/miriam_diamond_atlas.jpg)'
  focal_point: ""
  preview_only: false

links:
- icon: rss
  icon_pack: fas
  name: Project Website
  url:  https://atlas.cern/
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: 
---

For this project, I was fortunate enough to participate in [CERN's Summer Student Programme](https://home.cern/summer-student-programme).
This program is focused on introducing undergraduates across the world to the tools required to do experimental high energy physics.
Because of my fluency in C++, I was assigned to work on a software project where I help develop a Data Quality Monitoring (DQM) system for the [ATLAS Forward Proton (AFP) Detector](https://www.sciencedirect.com/science/article/pii/S2405601415006744). 

This DQM system was focused on how the AFP Detector would respond to different settings for the Large Hadron Collidor Beam; these settings are colloquially referred to as "Optics" because of the beams similarity to optical beams. Before this project, the Optics Studies were dispersed across many different codes throughout the AFP group. I was given a lot of code by my advisor and his colleagues, which I was able to unite into one framework that took in the optics settings given to the AFP group by the beam runners and produced dozens of configuration plots by simulating the path of the beam under these settings.

Below is an example of one of the plots that my executable was able to produce.
This plot shows the parameters that a proton in the beam would have to have in order to be able to hit the AFP detector.
These parameters are highly correlated with the hundreds of optics settings, so this was very important for determining how the AFP detector would be able to function.
![Example of a Generate Optics Studies Plot](exampleresult.png)

My advisor for this project was [Maciej Trzebinski](https://arxiv.org/abs/1503.04936v1) representing the AFP Group.
