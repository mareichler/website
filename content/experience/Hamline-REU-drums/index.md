---
title: The Search for Harmonic Drums
subtitle: Hamline University (Summer 2016)
summary: Summer 2016
tags:
- research
#set date 
date:    "2015-08-01T00:00:00Z"
#update with last modify (based on commits to github page for this file)
lastmod: "2019-04-17T00:00:00Z"


# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  placement: 2
  caption: ''
  focal_point: ""
  preview_only: true

links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: 
---

This was my second summer of research, and I was very excited to learn more about simulating physics and tying that in to real-world experiments.

In this project, I developed a package in [Mathematica](https://www.wolfram.com/mathematica/) that could take an input "shape" of a drum (a two-dimensional, closed loop) and calculate the eigenvalues of the two-dimensional wave equation using [NDEigensystem](https://reference.wolfram.com/language/ref/NDEigensystem.html).
This calculation of the frequencies of the drum was then re-introduced to Mathematica's optimization algorithms to search for drum shapes that had resonant frequencies close to being harmonic.

I led the computational side of this project while other undergraduates that I was working with studied the physical implementation of the shapes I found.

My advisor for this project was [Prof. Andy Rundquist](https://www.hamline.edu/faculty-staff/andy-rundquist/) from Hamline University.
