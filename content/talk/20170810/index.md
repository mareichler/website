---
title: Optics Studies for the ATLAS Forward Proton Project
event: Summer Student End-Of-Summer Presentations
event_url: https://indico.cern.ch/event/647235/

location: CERN
address:
  city: Geneva
  region: Switzerland
#  region: TN
#  postcode: '94305'

subtitle: Oral presentation
summary: Oral presentation
abstract: A brief summary of my accomplishments during the REU program at CERN.

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2017-08-10T14:00:00Z"
#date_end: "2017-06-01T16:20:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
#publishDate: "2017-01-01T00:00:00Z"

authors: []
tags: 

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: true

links:
url_code: ""
url_pdf: ""
url_slides: "slides.pdf"
url_video: ""

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
- CERN-REU

# Enable math on this page?
math: true
---
